package com.example.estelle.cs_akazoo_app.features.playlists;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.estelle.cs_akazoo_app.R;

public class PlaylistsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_playlists);

        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.playlists_root, new PlaylistsFragment())
                .commit();
    }
}
