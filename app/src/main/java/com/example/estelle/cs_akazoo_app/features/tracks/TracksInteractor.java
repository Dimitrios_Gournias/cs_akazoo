package com.example.estelle.cs_akazoo_app.features.tracks;

import java.util.ArrayList;

public interface TracksInteractor {

    void getTracks(OnTracksFinishListener listener, String playlistId);

    interface OnTracksFinishListener {
        void onSuccess(ArrayList<TrackDomain> tracks);

        void onError();
    }
}
