package com.example.estelle.cs_akazoo_app.features.playlists;

import com.example.estelle.cs_akazoo_app.R;

import java.util.ArrayList;

public class PlaylistsPresenterImpl implements PlaylistsPresenter,
        PlaylistsInteractor.OnPlaylistsFinishListener {

    PlaylistsView playlistsView;
    PlaylistsInteractor interactor;

    public PlaylistsPresenterImpl(PlaylistsView playlistsView) {
        this.playlistsView = playlistsView;
        interactor = new PlaylistsInteractorImpl();
    }

    @Override
    public void getPlaylists() {
        interactor.getPlaylists(this);
    }

    @Override
    public void getFilteredPlaylists(String filterString) {
        interactor.getFilteredPlaylists(this, filterString);
    }


    @Override
    public void onSuccess(ArrayList<PlaylistDomain> playlists) {
        ArrayList<PlaylistUI> playlistsUI = new ArrayList<>();
        if (playlists != null && !playlists.isEmpty()) {
            for (PlaylistDomain playlist : playlists) {
                PlaylistUI playlistUI = new PlaylistUI(
                        playlist.getPlaylistId(),
                        playlist.getName(),
                        playlist.getItemCount()
                );

                if (playlistUI.getItemCount() > 40)
                    playlistUI.setColorId(R.color.red);
                else
                    playlistUI.setColorId(R.color.blue);
                playlistsUI.add(playlistUI);
            }
        }

        playlistsView.showPlaylists(playlistsUI);
    }

    @Override
    public void onError() {
        playlistsView.showGeneralError();
    }
}
