package com.example.estelle.cs_akazoo_app.features.tracks;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.estelle.cs_akazoo_app.R;
import com.example.estelle.cs_akazoo_app.features.playlists.PlaylistUI;

import timber.log.Timber;

public class TracksActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tracks);

        PlaylistUI playlist = getIntent().getParcelableExtra("playlistId");
        Timber.e("The playlist id : " + playlist.getPlaylistId());

        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.tracks_root, TracksFragment.newInstance(playlist))
                .commit();


    }
}
