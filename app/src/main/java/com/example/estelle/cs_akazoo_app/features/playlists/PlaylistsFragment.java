package com.example.estelle.cs_akazoo_app.features.playlists;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.example.estelle.cs_akazoo_app.R;
import com.example.estelle.cs_akazoo_app.features.tracks.TracksActivity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import timber.log.Timber;

/**
 * A simple {@link Fragment} subclass.
 */
public class PlaylistsFragment extends Fragment implements PlaylistsView {

    @BindView(R.id.playslists_rv)
    RecyclerView playlistsRv;
    @BindView(R.id.filter_edit_text)
    EditText mFilterEditText;

    PlaylistsPresenter presenter;

    public PlaylistsFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_playlists, container, false);
        ButterKnife.bind(this, v);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        playlistsRv.setLayoutManager(layoutManager);
        presenter = new PlaylistsPresenterImpl(this);
        presenter.getPlaylists();
        return v;
    }

    @Override
    public void showPlaylists(ArrayList<PlaylistUI> playlists) {
        PlaylistsRvAdapter playlistsRvAdapter = new PlaylistsRvAdapter(playlists, new OnPlaylistClickListener() {
            @Override
            public void onPlaylistClicked(PlaylistUI playlist) {
                Intent intent = new Intent(getActivity(), TracksActivity.class);
                intent.putExtra("playlistId", playlist);
                getActivity().startActivity(intent);
            }
        }, getActivity());
        playlistsRv.setAdapter(playlistsRvAdapter);
    }

    @Override
    public void showGeneralError() {
        Toast.makeText(getContext(), getString(R.string.general_error_message), Toast.LENGTH_LONG).show();
    }

    @OnClick(R.id.filter_button)
    public void filterPlaylists(View view) {
        String filterText = mFilterEditText.getText().toString();
        presenter.getFilteredPlaylists(filterText);
    }
}
