package com.example.estelle.cs_akazoo_app.features.tracks;

import com.example.estelle.cs_akazoo_app.rest.RestClient;
import com.example.estelle.cs_akazoo_app.rest.responses.PlaylistsResponse;
import com.example.estelle.cs_akazoo_app.rest.responses.TracksResponse;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TracksInteractorImpl implements TracksInteractor{

    @Override
    public void getTracks(final OnTracksFinishListener listener, String playlistId) {

        Call<TracksResponse> call = RestClient.call().fetchTracks(playlistId);

        call.enqueue(new Callback<TracksResponse>() {
            @Override
            public void onResponse(Call<TracksResponse> call, Response<TracksResponse> response) {
                ArrayList<TrackDomain> tracks = response.body().getResult().getItems();
                listener.onSuccess(tracks);
            }

            @Override
            public void onFailure(Call<TracksResponse> call, Throwable t) {

            }
        });


    }
}
