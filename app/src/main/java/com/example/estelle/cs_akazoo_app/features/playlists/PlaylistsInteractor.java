package com.example.estelle.cs_akazoo_app.features.playlists;

import java.util.ArrayList;

public interface PlaylistsInteractor {

    void getPlaylists(OnPlaylistsFinishListener listener);

    void getFilteredPlaylists(OnPlaylistsFinishListener listener, String filterString);

    interface OnPlaylistsFinishListener {

        void onSuccess(ArrayList<PlaylistDomain> playlists);

        void onError();

    }

}
