package com.example.estelle.cs_akazoo_app.features.playlists;

import com.example.estelle.cs_akazoo_app.rest.RestClient;
import com.example.estelle.cs_akazoo_app.rest.responses.PlaylistsResponse;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class PlaylistsInteractorImpl implements PlaylistsInteractor {


    @Override
    public void getPlaylists(final OnPlaylistsFinishListener listener) {

        Call<PlaylistsResponse> call = RestClient.call().fetchPlaylists();
        call.enqueue(new Callback<PlaylistsResponse>() {

            @Override
            public void onResponse(Call<PlaylistsResponse> call, Response<PlaylistsResponse> response) {
                try {
                    listener.onSuccess(response.body().getResult());
                } catch (Exception e) {
                    onFailure(call, e);
                }
            }

            @Override
            public void onFailure(Call<PlaylistsResponse> call, Throwable t) {
                Timber.e("Failed to fetch playlists from the server");
                listener.onError();
            }

        });
    }

    @Override
    public void getFilteredPlaylists(final OnPlaylistsFinishListener listener, final String filterString) {

        Call<PlaylistsResponse> call = RestClient.call().fetchPlaylists();
        call.enqueue(new Callback<PlaylistsResponse>() {
            @Override
            public void onResponse(Call<PlaylistsResponse> call, Response<PlaylistsResponse> response) {
                ArrayList<PlaylistDomain> filteredPlaylists = new ArrayList<>();
                ArrayList<PlaylistDomain> playlists = response.body().getResult();
                for (int i = 0; i < playlists.size(); i++) {
                    if (playlists.get(i).getName().startsWith(filterString))
                        filteredPlaylists.add(playlists.get(i));
                }
                listener.onSuccess(filteredPlaylists);
            }

            @Override
            public void onFailure(Call<PlaylistsResponse> call, Throwable t) {
                listener.onError();
            }
        });


    }
}
