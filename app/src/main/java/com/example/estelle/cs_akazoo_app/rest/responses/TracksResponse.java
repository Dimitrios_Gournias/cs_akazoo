package com.example.estelle.cs_akazoo_app.rest.responses;

import com.example.estelle.cs_akazoo_app.features.tracks.TrackDomain;

import java.util.ArrayList;

public class TracksResponse {

    private TracksResponseItem Result;

    public TracksResponseItem getResult() {
        return Result;
    }

    public void setResult(TracksResponseItem result) {
        Result = result;
    }
}
