package com.example.estelle.cs_akazoo_app.features.tracks;

public class TrackDomain {

    private int ItemId;
    private String TrackName;
    private String ArtistName;
    private String ImageUrl;

    public TrackDomain(int itemId, String trackName, String artistName, String imageUrl) {
        ItemId = itemId;
        TrackName = trackName;
        ArtistName = artistName;
        ImageUrl = imageUrl;
    }

    public int getItemId() {
        return ItemId;
    }

    public void setItemId(int itemId) {
        ItemId = itemId;
    }

    public String getTrackName() {
        return TrackName;
    }

    public void setTrackName(String trackName) {
        TrackName = trackName;
    }

    public String getArtistName() {
        return ArtistName;
    }

    public void setArtistName(String artistName) {
        ArtistName = artistName;
    }

    public String getImageUrl() {
        return ImageUrl;
    }

    public void setImageUrl(String imageUrl) {
        ImageUrl = imageUrl;
    }
}
