package com.example.estelle.cs_akazoo_app.features.tracks;

import java.util.ArrayList;

public interface TracksView {

    void showTracks(ArrayList<TrackUI> tracks);

}
