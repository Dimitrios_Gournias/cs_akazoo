package com.example.estelle.cs_akazoo_app.features.playlists;

import java.util.ArrayList;

public interface PlaylistsPresenter {

    void getPlaylists();

    void getFilteredPlaylists(String filter);

}
